package com.example.hw1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        val generateRandomNumberButton = findViewById<Button>(R.id.generateRandomNumberButton)
        val randomNumberTextView = findViewById<TextView>(R.id.randomNumberTextView)
        val check = findViewById<TextView>(R.id.check)
        generateRandomNumberButton.setOnClickListener{
            d("ButtonClick","It is Random Generator")
            val number = randomNumber()
            d("buttonTextView","$number")
            randomNumberTextView.text = number.toString()
            if (number > 0 && number % 5 == 0) {check.text = "Yes"}
            else {check.text = "No"}
        }
    }

    private fun randomNumber(): Int {
        val number:Int = (1..100).random()
        return number
    }
}